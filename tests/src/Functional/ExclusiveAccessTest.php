<?php

declare(strict_types=1);

namespace Drupal\Tests\exclusiv_access\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use PHPUnit\Framework\Attributes\Group;

/**
 * Functional tests for ExclusiveAccess.
 */
#[Group('exclusiv_access')]
class ExclusiveAccessTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * AccessUser.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $accessUser;

  /**
   * Admin User.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['exclusiv_access', 'field_ui', 'node'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set up content Type.
    $this->contentTypeSetUp();

    // Set up "reader" user.
    $this->accessUser = $this->drupalCreateUser(['see content without token']);

    // Set up "author" user.
    $this->adminUser = $this->drupalCreateUser([
      'create test content',
      'edit any test content',
      'see content without token',
    ], 'author');
  }

  /**
   * Test callback.
   */
  public function testFunctional(): void {

    // Log as author.
    $this->DrupalLogin($this->adminUser);
    // Create Test Node.
    $this->createNode(['type' => 'test', 'title' => 'Test_EA']);
    $this->DrupalLogout();

    // The node is by default visible for anonymous.
    $this->drupalGet('/node/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Test_EA');

    // Log as author.
    $this->DrupalLogin($this->adminUser);
    // Activate Exclusiv Access.
    $this->DrupalGet('/node/1/edit');
    $page = $this->getSession()->getPage();
    // Check box.
    $findName = $page->find("css", '#edit-field-ea-0-value');
    $findName->check();
    // Submit form.
    $findName = $page->find("css", '#node-test-edit-form');
    $findName->submit();

    // See validation message.
    $this->assertSession()->pageTextContains('Exclusiv URL : ');

    // Recover token.
    $this->DrupalGet('/node/1/edit');
    $page = $this->getSession()->getPage();
    $findName = $page->find("css", '#edit-field-ea-0-token');
    $token = $findName->getValue();
    $this->assertNotEmpty($token);

    // Anonymous get now a 404.
    $this->DrupalLogout();
    $this->drupalGet('/node/1');
    $this->assertSession()->statusCodeEquals(404);

    // Anonymous get 200 with the token.
    $this->drupalGet('/node/1', ['query' => ['token' => $token]]);
    $this->assertSession()->statusCodeEquals(200);

    // Reader with permission access it.
    $this->DrupalLogin($this->accessUser);
    $this->drupalGet('/node/1');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Setup Content Type.
   */
  protected function contentTypeSetUp() {

    // Create Content Type.
    $this->drupalCreateContentType(['type' => 'test', 'name' => 'Test']);

    FieldStorageConfig::create([
      'field_name' => 'field_ea',
      'entity_type' => 'node',
      'type' => 'exclusiv_access_field_type',
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_ea',
      'entity_type' => 'node',
      'label' => 'EA',
      'bundle' => 'test',
      'widget' => 'exclusiv_access_field_widget',
    ])->save();

    $form_display = \Drupal::service('entity_display.repository')->getFormDisplay('node', 'test');
    $form_display = $form_display->setComponent('field_ea', ['region' => 'content']);
    $form_display->save();
  }

}
