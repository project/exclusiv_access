<?php

/**
 * @file
 * Contains exclusiv_access.help pages.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function exclusiv_access_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the exclusiv_access module.
    case 'help.page.exclusiv_access':
      $output = '<h2>' . t('About') . '</h2>';
      $output .= '<p>' . t('This module provide a light access control solution.') . '</p>';
      $output .= '<p>' . t('There is not a "global access control" or a "access security solution" but a "content-by-content" solution') . '</p>';
      $output .= '<h3>' . t('Use Case') . '</h3>';
      $output .= '<ol>';
      $output .= '<li>' . t('The use case are a little site, without real dev team, without client access control.') . '</li>';
      $output .= '<li>' . t('The site have a minimalist syndication like "manuel newsletter" and admin want share new content with newsletter subscriber first.') . '</li>';
      $output .= '<li>' . t('On these contents, admin activate Exclusiv Access, and share content with token to subscribers.') . '</li>';
      $output .= '<li>' . t('Several days after, admins will deactivate Exclusiv Access, and content are full access.') . '</li>';
      $output .= '</ol>';
      $output .= '<h3>' . t('How use it') . '</h3>';
      $output .= '<p>' . t('Just follow steps:') . '</p>';
      $output .= '<ol>';
      $output .= '<li>' . t('Activate module') . '</li>';
      $output .= '<li>' . t('Edit an fieldable entity bundle and add the "Exclusiv Access" field.') . '</li>';
      $output .= '<li>' . t('When creating or editing content, open "Exclusiv access control" tab.') . '</li>';
      $output .= '<li>' . t('Save content, and a message show you the tokenized URL.') . '</li>';
      $output .= '</ol>';
      $output .= '<h3>' . t('Permission') . '</h3>';
      $output .= '<p>' . t('A custom permission are present for seeing "Exclusiv" content without token.') . '</p>';
      return $output;

    default:
  }
}
