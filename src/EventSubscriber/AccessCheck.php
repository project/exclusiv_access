<?php

namespace Drupal\exclusiv_access\EventSubscriber;

use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\State\State;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\EventSubscriber\DefaultExceptionHtmlSubscriber;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Class EditAdAccessEventSubscriber.
 */
class AccessCheck implements EventSubscriberInterface {

  /**
   * Current User.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Service current_route_match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Service exception.default_html.
   *
   * @var \Drupal\Core\EventSubscriber\DefaultExceptionHtmlSubscriber
   */
  protected $exceptionDefaultHtml;

  /**
   * List of keys.
   *
   * @var array
   */
  protected $exclusivAccesses;

  /**
   * Token in request.
   *
   * @var string
   */
  protected $token;

  /**
   * Constructs a new EditAdAccessEventSubscriber object.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    CurrentRouteMatch $current_route_match,
    DefaultExceptionHtmlSubscriber $exception_default_html,
    State $state,
    RequestStack $stack,
  ) {

    $this->currentUser = $current_user;
    $this->currentRouteMatch = $current_route_match;
    $this->exceptionDefaultHtml = $exception_default_html;
    $this->exclusivAccesses = $state->get('exclusiv_access');
    $this->token = $stack->getCurrentRequest()->query->get('token');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {

    $events[KernelEvents::REQUEST][] = ['exclusivAccess'];
    return $events;

  }

  /**
   * Ad accessDenied Event subscriber.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Event for interception.
   */
  public function exclusivAccess(RequestEvent $event) {

    // Try to find a entity from route.
    $entity = $this->getEntityFromRoute();
    if ($entity != NULL) {

      $entity_type = $entity->getEntityType()->id();

      // The entity are in exclusiv access.
      if (isset($this->exclusivAccesses[$entity_type][$entity->id()])
        && !$this->currentUser->hasPermission('see content without token')) {

        // If no valid token.
        if ($this->token != $this->exclusivAccesses[$entity_type][$entity->id()]) {

          // Return to 404.
          throw new NotFoundHttpException();
        }
      }
    }
  }

  /**
   * Extract Entity from current route.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   Entity or NULL.
   */
  private function getEntityFromRoute() {
    // Entity will be found in the route parameters.
    if (($route = $this->currentRouteMatch->getRouteObject()) && ($parameters = $route->getOption('parameters'))) {
      // Determine if the current route represents an entity.
      foreach ($parameters as $name => $options) {
        if (isset($options['type']) && strpos($options['type'], 'entity:') === 0) {
          $entity = $this->currentRouteMatch->getParameter($name);
          if ($entity instanceof ContentEntityInterface && $entity->hasLinkTemplate('canonical')) {
            return $entity;
          }

          // Since entity was found, no need to iterate further.
          return NULL;
        }
      }
    }
  }

}
