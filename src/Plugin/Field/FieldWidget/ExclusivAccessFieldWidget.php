<?php

namespace Drupal\exclusiv_access\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\State;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'exclusiv_access_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "exclusiv_access_field_widget",
 *   label = @Translation("Exclusiv access"),
 *   field_types = {
 *     "exclusiv_access_field_type"
 *   }
 * )
 */
class ExclusivAccessFieldWidget extends WidgetBase {

  /**
   * List of keys.
   *
   * @var array
   */
  protected $exclusivAccesses;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param Drupal\Core\State\State $state
   *   State API.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, State $state) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->exclusivAccesses = $state->get('exclusiv_access');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element += [
      '#type' => 'details',
    ];

    $element['#title'] = $this->t('Exclusiv Access Control');

    $element['value'] = [
      '#type' => 'checkbox',
      '#title' => 'activate',
      '#default_value' => $items[$delta]->value ?? FALSE,
    ];

    $entity_type = $items->getEntity()->getEntityType()->id();
    $entity_id = $items->getEntity()->id();

    if ($entity_id != NULL) {
      if (isset($this->exclusivAccesses[$entity_type][$entity_id])) {
        $element['token'] = [
          '#title' => 'token',
          '#type' => 'textfield',
          '#default_value' => $this->exclusivAccesses[$entity_type][$entity_id],
          '#disabled' => TRUE,
        ];
      }
    }

    $element['#group'] = 'advanced';

    return $element;
  }

}
