<?php

namespace Drupal\exclusiv_access\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'exclusiv_access_field_type' field type.
 *
 * @FieldType(
 *   id = "exclusiv_access_field_type",
 *   label = @Translation("Exclusiv access"),
 *   description = @Translation("A boolean field for activating exclusiv access"),
 *   default_widget = "exclusiv_access_field_widget",
 *   default_formatter = "exclusiv_access_field_formatter",
 * )
 */
class ExclusivAccessFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties = [];

    $properties['value'] = DataDefinition::create('boolean')
      ->setLabel(t('activate'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'size' => 'tiny',
        ],
      ],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['value'] = (time() % 2 == 1) ? TRUE : FALSE;
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) : bool {
    parent::postSave($update);

    // If checked.
    if ($this->value == 1) {

      $entity_type = $this->getEntity()->getEntityType()->id();
      $entity_id = $this->getEntity()->id();
      $random = new Random();

      // Create & save token.
      $state = \Drupal::state()->get('exclusiv_access');
      if (!isset($state[$entity_type][$entity_id])) {
        $state[$entity_type][$entity_id] = $random->name(32);
        \Drupal::state()->set('exclusiv_access', $state);
      }

      // Print message.
      $token = $state[$entity_type][$entity_id];
      $url = $this->getEntity()->toUrl()->setAbsolute()->toString();
      $link = $url . '?token=' . $token;
      \Drupal::messenger()->addMessage($this->t('Exclusiv URL : @link', ['@link' => $link]));

      return TRUE;
    }
    return FALSE;
  }

}
