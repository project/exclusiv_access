<?php

namespace Drupal\exclusiv_access\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'exclusiv_access_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "exclusiv_access_field_formatter",
 *   module = "exclusiv_access",
 *   label = @Translation("Empty formatter"),
 *   field_types = {
 *     "exclusiv_access_field_type"
 *   }
 * )
 */
class ExclusivAccessFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Does not actually output anything.
    return [];
  }

}
